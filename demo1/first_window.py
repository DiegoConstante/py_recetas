from typing import Optional, Tuple
import arcade
import pyglet

class MyFirstWindow(arcade.Window):


    # --- CREACION DE LA VENTANA O PANTALLA DEL CELULAR
    def __init__(self, width: int = 800, height: int = 600, title: str = 'Arcade Window'):
        super().__init__(width, height, title)

        # -- LUGAR DONDE SE ABRE LA VENTANA
        self.set_location(50,50)

        # -- COLOR DEL FONDO DE LA VENTANA
        arcade.set_background_color((255,140,0))

    # --- RENDERIZADO DE LA PANTALLA 
    def on_draw(self):
        arcade.start_render()

        # -- Texto
        arcade.draw_text("Recetas de Cocina", 80, 600, (254,255,255), 20, 5, "left", "calibri", True, False, "left", "baseline", False, 0)
        return super().on_draw()